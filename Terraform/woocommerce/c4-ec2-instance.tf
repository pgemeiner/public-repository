
data "template_file" "create_db" {
  template = <<EOF
create database wordpress;
  EOF
}

# Create EC2 Instance - Amazon Linux
resource "aws_instance" "lamp" {
  ami           = data.aws_ami.amzlinux.id
  instance_type = var.instance_type
  key_name      = "terraform-key-frankfurt"
  #count = terraform.workspace == "default" ? 1 : 1
  user_data              = file("apache-install.sh")
  vpc_security_group_ids = ["sg-0e98d42e509e173ae", "sg-0e7d0fa655c09bec6", "sg-0bbb1182cafc4ac60"]
  tags = {
    "Name" = "Woocommerce-${terraform.workspace}-0"
  }

  # Connection Block for Provisioners to connect to EC2 Instance
  connection {
    type        = "ssh"
    host        = self.public_ip # Understand what is "self"
    user        = "ec2-user"
    password    = ""
    private_key = file("private-key/terraform-key-frankfurt.pem")
  }

  # Copies the file-copy.html file to /tmp/file-copy.html
  provisioner "file" {
    source      = "apps/file-copy.html"
    destination = "/tmp/file-copy.html"
  }

  provisioner "file" {
    source      = "apps/httpd.conf"
    destination = "/tmp/httpd.conf"
  }

  provisioner "file" {
    source      = "apps/.htaccess"
    destination = "/tmp/.htaccess"
  }

  # Copies the file to Apache Webserver /var/www/html directory
  provisioner "remote-exec" {
    inline = [
      "sleep 120", # Will sleep for 120 seconds to ensure Apache webserver is provisioned using user_data
      "sudo cp /tmp/file-copy.html /var/www/html",
      "sudo cp /tmp/httpd.conf /etc/httpd/conf",
      "sudo cp /tmp/.htaccess /var/www/html",
      "sudo chmod 0666 /var/www/html/.htaccess",
      "sudo systemctl restart httpd"
    ]
  }

  # Install MariaDB
  provisioner "remote-exec" {
    inline = [
      "sudo amazon-linux-extras install -y mariadb10.5",
      "sudo systemctl start mariadb",
      "sudo systemctl enable mariadb",
      "sudo systemctl is-enabled mariadb"
    ]
  }

  # Setup MariaDB
  # Install Wordpress
  # Install Woocommerce
  provisioner "remote-exec" {
    inline = [
      "echo -e \"\ny\ny\n${var.dbpassw}\n${var.dbpassw}\ny\ny\ny\ny\n\" | sudo /usr/bin/mysql_secure_installation", // mysql passw
      "cat > create_db.sql <<EOL\n${data.template_file.create_db.rendered}\nEOL",                                   // mysql create db wordpress
      "mysql -u root -p${var.dbpassw} < create_db.sql",
      "curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar", // install wordpress
      "chmod +x wp-cli.phar",
      "sudo mv wp-cli.phar /usr/local/bin/wp",
      "wp core download",
      "wp config create --dbname=wordpress --dbuser=root --dbpass=${var.dbpassw} --dbhost=localhost --dbprefix=wp_",
      "wp core install --url=${aws_instance.lamp.public_ip} --title=page --admin_user=${var.dbuser} --admin_password=${var.dbpassw} --admin_email=office@visualsearch.at",
      "sudo mv * /var/www/html/",
      "wp rewrite structure '/%postname%/' --path=/var/www/html",
      "wp plugin install woocommerce --activate --path=/var/www/html", // install woocommerce
      "sudo chmod -R 0777 /var/www/html/wp-content/uploads/"
    ]
    on_failure = continue
  }

}

resource "aws_ec2_instance_state" "woocommerce" {
  instance_id = aws_instance.lamp.id
  state       = "running"
  //state       = "stopped"
}
