# Input Variables
variable "aws_region" {
  description = "Region in which AWS Resources to be created"
  type        = string
  default     = "eu-central-1"
}

variable "instance_type" {
  description = "EC2 Instance Type - Instance Sizing"
  type        = string
  #default = "t2.micro"
  default = "t2.small"
}

variable "dbpassw" {
  type        = string
  description = "DB Password set from environment"
  sensitive   = true
}

variable "dbuser" {
  type        = string
  description = "DB User set from environment"
  sensitive   = true
}