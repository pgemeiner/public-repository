# Terraform
Collection of Terraform Config Files

## Modules

Module that creates AWS S3 bucket. Fixed deprecated AWS S3 rights.

## Woocommerce

Terraform example on how to deploy Woocommerce on a AWS EC2 instance

A CloudFormation alternative can be found [here](https://github.com/VisualSearch-GmbH/CloudFormation)

