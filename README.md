<h2> Hi, I'm Peter Gemeiner! <img src="https://media.giphy.com/media/BLy7N6MJNYCeMeuB18/giphy.gif" width="50"></h2>
<img align='right' src="https://media2.giphy.com/media/v1.Y2lkPTc5MGI3NjExb2FkOGhvMnFqeTZ0bWkxZ2Y4M3cyeGxjbWttaG04aHAzc2M1aG55OSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/0lGd2OXXHe4tFhb7Wh/giphy.gif" width="175">
<p><em>AI/Machine Learning and Cloud Engineer</em></p>

[![alt text](img/markdown_logo.png "Gitlab"){width=5%}](https://gitlab.com/pgemeiner/public-repository)
[![alt text](img/linkedin_logo.png "Linkedin"){width=5%}](https://www.linkedin.com/in/peter-gemeiner-bb64a254/)

### <img src="https://media.giphy.com/media/5QQpfPOJEnkeK7tTBr/giphy.gif" width="50"> A little more about me... 

```python

class Peter(Human):
    pronouns = ("He", "Him")
    code = ["Python🐍", "C/C++", "Java", "PHP", "JavaScript", "SQL", "HTML"]
    ide = ("intellij")

    technologies = {
        "ai_and_machine_learning": {
            "tools": ["tensorflow", "keras", "opencv", "sklearn", "pandas", ...],
            "concepts": ["Image Processing", "Natural Language Processing"],
        },
        "web": {
            "front_end": {
                "js": ["React"],
                "css": ["materialize", "bootstrap"]
            },
            "back_end": {
                "python": ["flask", "flask_restx", "fastapi"],
                "js": ["node"],
            },
        }
        "cloud": ["AWS", "Terraform", "Docker🐳", "Kubernetes"],
        "databases": ["MySql", "DynamoDB", "Elastic", "sqlite"],
    }


if __name__ == "__main__":
    from time import sleep

    from kitchen import coffee
    import laptop
    import work

    peter = Peter()

    while True:
        sleep(8 * 60 * 60)
        workload = work.get_daily_workload()
        while len(workload) > 0:
            coffee(peter)
            laptop.code(peter)

```

---
