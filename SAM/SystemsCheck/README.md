# SystemsCheck

Check the status of all webpages and important endpoints

```bash
sam build --use-container
sam deploy --guided
```

```bash
SystemsCheck$ sam local invoke SystemsCheckFunction --event events/event.json
```

```bash
SystemsCheck$ sam local start-api
SystemsCheck$ curl http://localhost:3000/
```