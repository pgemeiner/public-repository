
import json
from main.app import lambda_handler


class Context:
    def __init__(self, arn: str):
        self.invoked_function_arn = arn


if __name__ == "__main__":
    event = {}
    with open("events/event.json", 'r') as f:
        event = json.load(f)
        ret = lambda_handler(event, Context("ARN"))