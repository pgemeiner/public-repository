
import boto3
import http.client
import json
import os
from urllib.request import urlopen


ssm = boto3.client("ssm")
sns = boto3.client("sns")


class SystemsCheckException(Exception):
    pass


def read_webpage(url):
    """
    Reads the content of a webpage from the given URL.

    Parameters:
    url (str): The URL of the webpage to be read.

    Returns:
    str: The content of the webpage in UTF-8 format.

    Raises:
    SystemsCheckException: If an error occurs while reading the webpage.

    """
    try:
        page = urlopen(url)
        html_bytes = page.read()
        return html_bytes.decode("utf-8")
    except Exception as error:
        SystemsCheckException(error)



def search_single():
    """

    Searches a single image using a REST API.

    Returns:
        True if the search is successful and the API returns a status code of 200,
        False otherwise.

    Raises:
        SystemsCheckException: If there is any error during the search process.
    """

    try:

        api_key = ssm.get_parameter(Name=os.environ["ApiKey"])["Parameter"]["Value"]
        endpoint = ssm.get_parameter(Name=os.environ["EndPoint"])["Parameter"]["Value"]
        host = ssm.get_parameter(Name=os.environ["Host"])["Parameter"]["Value"]
        image_data = ssm.get_parameter(Name=os.environ["ImageData"])["Parameter"]["Value"]

        endpoint = endpoint.replace("https://", "")
        connection_address = endpoint.split("/")

        conn = http.client.HTTPSConnection(connection_address[0])

        headers = {"Content-Type": "application/json",
                   "Vis-API-KEY": api_key,
                   "Vis-SYSTEM-HOSTS": host,
                   "Vis-SYSTEM-TYPE": "custom"}

        payload = json.dumps({"image_data": image_data})

        conn.request("POST", f"/{connection_address[1]}", headers=headers, body=payload)
        response = conn.getresponse()
        data = response.read()

        if response.code != 200:
            raise Exception("response not equal 200")
        return True
    except Exception as error:
        raise SystemsCheckException(error)


def lambda_handler(event, context):
    """
    Handles the lambda function that checks the visual search system.

    :param event: The event data passed to the lambda function.
    :type event: dict
    :param context: The runtime information of the lambda function.
    :type context: object
    :return: The response data to be returned by the lambda function.
    :rtype: dict
    """

    webpage = ssm.get_parameter(Name=os.environ["WebPage"])["Parameter"]["Value"]
    topic = ssm.get_parameter(Name=os.environ["SystemsCheckSNS"])["Parameter"]["Value"]

    try:
        content = read_webpage(webpage)
        if "home" not in content.lower():
            sns.publish(TopicArn=topic, Message="Bad news \n\n Oh, webpage failed!")
            print("ERROR")
            return {"statusCode": 500, "body": json.dumps({"message": "error"})}
    except:
        sns.publish(TopicArn=topic, Message="Bad news \n\n Oh, webpage failed!")
        print("ERROR")
        return {"statusCode": 500, "body": json.dumps({"message": "error"})}

    if search_single() == False:
        sns.publish(TopicArn=topic, Message="Bad news \n\n Oh, webpage failed!")
        print("ERROR")
        return {"statusCode": 500, "body": json.dumps({"message": "error"})}

    sns.publish(TopicArn=topic, Message="Good news \n\n Hi, all test successful")

    print("OK")
    return {"statusCode": 200, "body": json.dumps({"message": "success"})}
