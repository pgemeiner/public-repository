import boto3
import botocore.exceptions
import copy
import json
import urllib.parse
from datetime import date
from urllib3 import Timeout, PoolManager


# Get secrects from AWS SecrectsManager
def get_secrets(client, secret):
    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret)
        return json.loads(get_secret_value_response['SecretString'])
    except Exception as error:
        raise Exception("reading secrets")


# Main loop
def lambda_handler(event, context):
    today = date.today()
    dateformat = today.strftime("%Y-%d-%m")

    # check input parameters
    if "url" in event and \
            "secret_name" in event and \
            "region_name" in event and \
            "customer_id" in event and \
            "invoice_reference" in event and \
            "description" in event and \
            "paid_amount" in event:

        secret = event["secret_name"]
        region = event["region_name"]
    else:
        return {"code": 400, "msg": "Incorrect input parameters"}

    # create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name=region)

    # get secrets
    try:
        secrets = get_secrets(client, secret)
    except Exception as error:
        return {"code": 500, "msg": "Error " + str(error)}

    # get token
    if "access_token" in secrets:
        access_token = secrets["access_token"]
    else:
        return {"code": 500, "msg": "Eror no valid token"}

    # urllib3 post
    # (requests package is not available in AWS Lambda)
    try:
        timeout = Timeout(connect=10.0, read=None)
        http = PoolManager(timeout=timeout)

        data = {"invoiceDate": dateformat,
                "paidDate": dateformat,
                "customer": event["customer_id"],
                "invoiceReference": event["invoice_reference"],
                "dueDate": dateformat,
                "paidContraAccount": event["paid_amount"],
                "description": event["description"],
                "currency": "USD",
                "currencyRate": "2",
                "lines": [
                    {
                        "account": "4000",
                        "amount": 10,
                        "amountType": "N"

                    }]
                }
        data = json.dumps(data)
        headers = {"Content-Type": "application/json",
                   "Authorization": "Bearer " + access_token}
        response = http.request('POST',
                                event["url"],
                                headers=headers,
                                body=data)
        response_json = json.loads(response.data)
    except Exception as error:
        return {"code": 500, "msg": "Error connecting to host " + str(error)}

    # check response
    if response.status == 200:
        return {
            "code": 201,
            "msg": "success",
            "response": response_json
        }
    else:
        return {"code": 500, "msg": "Error " + json.dumps(response.data, default=str)}
