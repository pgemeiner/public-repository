import boto3
import botocore.exceptions
import copy
import json
import urllib.parse
from urllib3 import Timeout, PoolManager


# Get secrects from AWS SecrectsManager
def get_secrets(client, secret):
    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret)
        return json.loads(get_secret_value_response['SecretString'])
    except Exception as error:
        raise Exception("reading secrets")


# Update secrets
def update_secrets(client, secret, secrets_prev):
    try:
        response = client.update_secret(
            SecretId=secret,
            SecretString=json.dumps(secrets_prev),
        )
        return None
    except Exception as error:
        raise Exception("updating secrets")


# Main loop
def lambda_handler(event, context):
    # update of FreeFinance refresh_token
    secret = "FreeFinance"
    region = "eu-west-1"

    # create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name=region)

    # get secrets
    try:
        secrets_prev = get_secrets(client, secret)
    except Exception as error:
        return {"code": 500, "msg": "Error " + str(error)}

    # get values
    if "client_id" in secrets_prev and \
            "refresh_token" in secrets_prev and \
            "token_url" in secrets_prev:

        client_id = secrets_prev["client_id"]
        refresh_token = secrets_prev["refresh_token"]
        token_url = secrets_prev["token_url"]
    else:
        return {"code": 500, "msg": "Eror no valid secrets"}

    # urllib3 post
    # (requests package is not available in AWS Lambda)
    try:
        timeout = Timeout(connect=10.0, read=None)
        http = PoolManager(timeout=timeout)
        data = {"grant_type": "refresh_token",
                "client_id": client_id,
                "refresh_token": refresh_token}
        data = urllib.parse.urlencode(data)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        response_update = http.request('POST',
                                       token_url,
                                       headers=headers,
                                       body=data)
        response_update_json = json.loads(response_update.data)
    except Exception as error:
        return {"code": 500, "msg": "Error connecting to host " + str(error)}

    # get tokens
    if response_update.status == 200 and \
            "access_token" in response_update_json and \
            "refresh_token" in response_update_json:

        secrets_now = copy.copy(secrets_prev)
        secrets_now["access_token"] = response_update_json["access_token"]
        secrets_now["refresh_token"] = response_update_json["refresh_token"]
    else:
        return {"code": 500, "msg": "Error " + str(response_update_json)}

    # update rehresh_token in secrets
    try:
        update_secrets(client, secret, secrets_now)
    except Exception as error:
        return {"code": 500, "msg": "Error " + str(error)}

    # get current secrets
    try:
        secrets_now = get_secrets(client, secret)
    except Exception as error:
        return {"code": 500, "msg": "Error " + str(error)}

    # exit
    return {
        "code": 200,
        "msg": "success",
        "secrects_previous": secrets_prev,
        "secret_now": secrets_now,
        "response_update": json.dumps(response_update.data, default=str)
    }

