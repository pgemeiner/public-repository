from datetime import datetime, timezone
import boto3


class DynamoTableGetFailed(Exception):
    pass


class DynamoTableUpdateFailed(Exception):
    pass


def get_date_time():
    return datetime.now(tz=timezone.utc).strftime("%Y%m%d-%H%M%S")


class DynamoTable(object):
    """
    DynamoTable class represents a table in DynamoDB and provides methods to interact with the table.

    Attributes:
        table (boto3.resource.Table): The DynamoDB table resource.
        url (str): The URL associated with the table.

    Methods:
        get_item(identifier):
            Retrieves the value for the given identifier from the table.
            Raises DynamoTableGetFailed exception if an error occurs.

        update_item(identifier, value):
            Updates the value for the given identifier in the table.
            Raises DynamoTableUpdateFailed exception if an error occurs.

        write_start():
            Writes the worker start timestamp to the table.

        write_status(message):
            Writes the worker status message with timestamp to the table.
    """

    def __init__(self, table_name, url):
        self.table = boto3.client("dynamodb", region_name="eu-west-1")
        self.table_name = table_name
        self.url = url

    def get_item(self, identifier, elem_format="S"):

        try:
            response = self.table.get_item(
                TableName=self.table_name,
                Key={"url": {"S": self.url}}
            )
        except Exception as error:
            raise DynamoTableGetFailed("ERROR getting etag " + str(error))

        return response["Item"][identifier][elem_format]

    def update_item(self, identifier, value):

        try:
            self.table.update_item(
                TableName=self.table_name,
                Key={"url": {"S": self.url}},
                UpdateExpression=f"set {identifier}=:{identifier}",
                ExpressionAttributeValues={
                    f":{identifier}": {"S": value}
                }
            )
        except Exception as error:
            raise DynamoTableUpdateFailed("ERROR updating table " + str(error))

    def write_start(self):
        self.update_item("f_worker_start", str(get_date_time()))

    def write_status(self, message):
        self.update_item("g_worker_status", str(get_date_time()) + message)
