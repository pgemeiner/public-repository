import boto3
import json


class S3ObjectDownloadFailed(Exception):
    pass


class S3ObjectUploadFailed(Exception):
    pass


class S3Object(object):
    """
    Class S3Object

    This class provides methods for interacting with objects in an S3 bucket.

    Methods:
        - get_etag(bucket, key): Gets the ETag of an object in the S3 bucket.
        - file_from_s3(bucket, key): Retrieves a file from the S3 bucket.
        - file_to_s3(bucket, key, data): Uploads a file to the S3 bucket.
        - json_from_s3(bucket, key): Retrieves a JSON object from the S3 bucket.
        - json_to_s3(bucket, key, data): Uploads a JSON object to the S3 bucket.
        - list_objects(bucket, prefix): Retrieves a list of objects with a specific prefix from the S3 bucket.
    """

    def __init__(self):
        self.s3 = boto3.client('s3')

    def get_etag(self, bucket, key):

        try:
            s3resp = self.s3.head_object(Bucket=bucket, Key=key)
        except Exception as error:
            raise S3ObjectDownloadFailed("ERROR getting s3 etag " + str(error))

        return s3resp["ETag"].replace('"', '')

    def file_from_s3(self, bucket, key):
        try:
            return self.s3.get_object(Bucket=bucket, Key=key)["Body"].read()
        except Exception as error:
            raise S3ObjectDownloadFailed("Failed to download file " + str(error))

    def file_to_s3(self, bucket, key, data):

        try:
            sentData = self.s3.put_object(
                Bucket=bucket,
                Key=key,
                Body=data,
                StorageClass="ONEZONE_IA")

            if sentData["ResponseMetadata"]["HTTPStatusCode"] != 200:
                raise S3ObjectUploadFailed(f"Failed to upload to bucket: {bucket}, key:{key}")
        except Exception as error:
            raise S3ObjectUploadFailed("Failed to upload file " + str(error))

    def json_from_s3(self, bucket, key):

        try:
            byte = self.s3.get_object(Bucket=bucket, Key=key)["Body"].read()

            byte = byte.decode("utf-8")
            byte = byte.replace("': '", '": "').replace("', '", '", "').replace("{'", '{"').replace("'}", '"}')
            byte = byte.replace("': \"", '": "').replace("', \"", '", "').replace("\", '", '", "').replace("'", '\\"')

            return json.loads(byte)
        except Exception as error:
            raise S3ObjectDownloadFailed("Failed to download file " + str(error))

    def json_to_s3(self, bucket, key, data):

        try:
            sentData = self.s3.put_object(
                Bucket=bucket,
                Key=key,
                Body=json.dumps(data),
                StorageClass="ONEZONE_IA")

            if sentData["ResponseMetadata"]["HTTPStatusCode"] != 200:
                raise S3ObjectUploadFailed(f"Failed to upload to bucket: {bucket}, key:{key}")
        except Exception as error:
            raise S3ObjectUploadFailed("Failed to upload file " + str(error))

    def list_objects(self, bucket, prefix):

        try:
            paginator = self.s3.get_paginator("list_objects_v2")
            pages = paginator.paginate(Bucket=bucket, Prefix=prefix, PaginationConfig={"PageSize": 500})

            objects = []
            for page in pages:
                if "Contents" in page:
                    for c in page["Contents"]:
                        if "Key" in c:
                            objects.append(c["Key"])

            return objects
        except Exception as error:
            raise S3ObjectDownloadFailed("ERROR getting s3 list of " + prefix + ": " + str(error))
