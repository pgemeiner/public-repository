# AWS CloudFormation
Collection of CloudFormation Stacks

## Ansible

Setup a simple Ansible playground with a master and two clients using CloudFormation

## Shopware 6.5

CloudFormation example on how to deploy Shopware 6.5 on a AWS EC2 instance

## Shopware 6.6

CloudFormation example on how to deploy Shopware 6.6 on a AWS EC2 instance

## WooCommerce

CloudFormation example on how to deploy WooCommerce on a AWS EC2 instance

A Terraform alternative can be found [here](https://github.com/VisualSearch-GmbH/Terraform)

